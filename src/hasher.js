const bcrypt = require("bcrypt")

const saltRound = 10

async function hash(password){
    return await bcrypt.hash(password, saltRound)
}

async function compare(password, hashPassword){
    return await bcrypt.compare(password, hashPassword);
}

module.exports = {
    hash,
    compare
}