const chai = require('chai')
const expect = chai.expect

var chaiAsPromised = require("chai-as-promised")
const requester = require('../requester.spec')

const Hasher = require("./hasher")

describe("Hasher Object", function(){
    describe("Unit tests", function(){
        it("should hash values", async function(){
            const testValue = "passwordTest"
            const hashedValue = await Hasher.hash(testValue)
            expect(hashedValue).to.not.equal(testValue)
        })
        it("should properly compare values", async function(){
            const testValue = "passwordTest"
            const hashedValue = await Hasher.hash(testValue)
            let result = await Hasher.compare(testValue, hashedValue)
            expect(result).to.be.equal(true)
            result = await Hasher.compare("notPassword", hashedValue)
            expect(result).to.be.equal(false)
        })
    })
})