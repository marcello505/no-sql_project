
const chai = require('chai')
const expect = chai.expect

const requester = require('../requester.spec')

describe('user journeys', function(){
    it("create account/profile, create writer, create book, create bookcopy, user loans book, user turns book back in.", async function(){
        let res
        let testAccount = {
            email: "test@user.nl",
            password: "password"
        }
        let testWriter = {
            name: "Ronald",
            description: "Mystery writer",
            dateOfBirth: "1954-01-01"
        }
        let testBook = {
            title: "Green Eggs",
            description: "This is a childrens book",
            ISBN: "1234567890",
            releaseDate: "1980-01-01",
            publisher: "Penguin Books",
            language: "English"
        }
        let testBookCopy = {
            canBeLoaned: true,
            isCurrentlyLoaned: false,
            dateOfEntry: "2021-04-10" 
        }

        res = await requester.post("/auth/register").send(testAccount)
        expect(res).to.have.status(201)
        res = await requester.post("/auth/login").send(testAccount)
        expect(res).to.have.status(200)
        const token = res.body.token
        const accountId = res.body.account._id
        res = await requester.post("/writer").set({ Authorization: `Bearer ${token}` }).send(testWriter)
        expect(res).to.have.status(201)
        testBook.writer = res.body._id
        res = await requester.post("/book").set({ Authorization: `Bearer ${token}` }).send(testBook)
        expect(res).to.have.status(201)
        testBookCopy.copyOf = res.body._id
        res = await requester.post("/bookcopy").set({ Authorization: `Bearer ${token}` }).send(testBookCopy)
        expect(res).to.have.status(201)
        const bookCopyToLoan = res.body._id
        res = await requester.post(`/bookcopy/loan/${bookCopyToLoan}`).set({ Authorization: `Bearer ${token}` }).send()
        expect(res).to.have.status(200)
        res = await requester.get(`/bookcopy/${bookCopyToLoan}`).send()
        expect(res).to.have.status(200)
        expect(res.body.isCurrentlyLoaned).to.be.equal(true)
        expect(res.body.loanedTo).to.be.equal(accountId)
        res = await requester.post(`/bookcopy/unloan/${bookCopyToLoan}`).set({ Authorization: `Bearer ${token}` }).send()
        expect(res).to.have.status(200)
        res = await requester.get(`/bookcopy/${bookCopyToLoan}`).send()
        expect(res).to.have.status(200)
        expect(res.body.isCurrentlyLoaned).to.be.equal(false)
        expect(res.body.loanedTo).to.equal(undefined)
    })
})