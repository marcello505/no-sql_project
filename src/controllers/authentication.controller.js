const { EntityNotFoundError } = require("../errors");
const errors = require("../errors");
const Account = require("../models/account.model")()
const Hasher = require("../hasher")
const jwt = require("jsonwebtoken");
const config = require("../config")
const neo = require("../../neo")

async function register(req, res, next){
    const newAccount = new Account(req.body)
    try{
        const session = neo.session()
        const neoQuery = "CREATE (p:Profile{ accountId: $id, name: $name, bio: $bio, favoriteBooks: []})"
        await newAccount.validate();
        newAccount.set({password: await Hasher.hash(newAccount.password)})
        await newAccount.save()
        await session.run(neoQuery, {id: String(newAccount._id), name: String(newAccount.email), bio: ""})
        res.status(201).json({status: "Complete", message: "Account was succesfully created."})
    }
    catch(err){
        next(err)
    }
}

async function login(req, res, next){
    const errorMessage = "Could not login, is everything correct?"
    try{
        const {email, password} = req.body
        const account = await Account.findOne({email: email})
        if(account){
            const passwordCorrect = await Hasher.compare(password, account.password)
            if(passwordCorrect){
                jwt.sign({accountId: account._id},
                    config.jwtKey,
                    {expiresIn: "48h"},
                    function(err, token){
                        if(err){
                            throw new EntityNotFoundError(err)
                        }
                        else if(token){
                            res.status(200).json({
                                stauts: "Login successful",
                                token,
                                account:{
                                    _id: account._id,
                                    email: account.email
                                }
                            })
                        }
                })
            }
            else{
                throw new EntityNotFoundError(errorMessage);
            }
        }
        else{
            throw new EntityNotFoundError(errorMessage)
        }
    }
    catch(err){
        next(err)
    }
}

async function verify(req, res, next){
    let token = req.headers.authorization;
    if (!token) {
        res.status(401).json({
            error: "Authorization header missing",
        });
    }
    else{
        token = token.substring(7, token.length);
        jwt.verify(token, config.jwtKey, (err, payload) => {
            if (err) {
                res.status(204).json({
                    error: "Could not authorize token"
                })
            }
            if (payload) {
                //Check if account still exists
                Account.findById(payload.accountId)
                    .then(result => {
                        if(result){
                            req.accountId = payload.accountId;
                            next();
                        }
                        else{
                            res.status(401).json({
                                error: "Could not find account"
                            })
                        }
                    })
                    .catch(err => {
                        //Some bs happened
                        res.status(500).send(err)
                    })
            }
        });
    }
}

async function userVerify(req, res, next){
    let token = req.headers.authorization;
    if(!token){
        res.status(401).json({
            error: "Authorization header missing",
        });
    }
    else{
        token = token.substring(7, token.length)
        jwt.verify(token, config.jwtKey, (err, payload) => {
            if (err) {
                res.status(204).json({
                    verified: false,
                    error: "Could not authorize token"
                })
            }
            if (payload) {
                //Check if account still exists
                Account.findById(payload.accountId)
                    .then(result => {
                        if(result){
                            result = {
                                verified: true,
                                account: {_id: result._id, email: result.email}
                            }
                            res.status(200).json(result)
                        }
                        else{
                            res.status(401).json({
                                error: "Could not find account"
                            })
                        }
                    })
                    .catch(err => {
                        //Some bs happened
                        res.status(500).send(err)
                    })
            }
        });
    }
    
}

module.exports = {
    register,
    login,
    verify,
    userVerify
}