const { EntityNotFoundError } = require("../errors");
const errors = require("../errors");
const Writer = require("../models/writer.model")();

async function create(req, res, next){
    const newWriter = new Writer(req.body)
    newWriter.save()
        .then((result) => {
            res.status(201).json(result)
        })
        .catch((err) => {
            next(err)
        });
}

async function getAll(req, res, next){
    Writer.find(req.query)
        .then((result) => {
            res.status(200).json(result)
        })
        .catch((err) => {
            next(err)
        })
}

async function getById(req, res, next){
    const id = req.params.writerId
    Writer.findById({ _id: id})
        .then((result) => {
            if(result){
                res.status(200).json(result);
            }
            else{
                throw new EntityNotFoundError("Writer not found")
            }
        })
        .catch((err) => {
            next(err)
        })
}

async function remove(req, res, next){
    const id = req.params.writerId;
    Writer.findByIdAndDelete(id)
        .then((result) => {
            if(result){
                res.status(200).json(result)
            }
            else{
                throw new EntityNotFoundError("Writer not found")
            }
        })
        .catch((err) => {
            next(err)
        })
}

async function update(req, res, next){
    const id = req.params.writerId;
    Writer.findByIdAndUpdate(id, req.body)
        .then((result) => {
            if(result){
                res.status(201).json(result)
            }
            else{
                throw new EntityNotFoundError("Writer not found")
            }
        })
        .catch((err) => {
            next(err)
        })
}

module.exports = {
    create,
    getAll,
    getById,
    remove,
    update
}