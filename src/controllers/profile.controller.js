const neo = require("../../neo")
const Account = require("../models/account.model")()
const Book = require("../models/book.model")()
const mongoose = require("mongoose")

async function remove(req, res, next){
    try{
        const session = neo.session()
        const neoQuery = "MATCH (p:Profile) WHERE p.accountId = $id DETACH DELETE p"
        const id = req.accountId
        //Check if user logged in is also the one deleting the account
        await Account.findByIdAndDelete(id)
        await session.run(neoQuery, {id: id})
        res.status(200).json({status: "Success", message: "Profile and account sucessfully deleted."})
    }
    catch(err){
        next(err)
    }
}

async function update(req, res, next){
    try{
        const session = neo.session();
        const querySearch = "MATCH (p:Profile) WHERE p.accountId = $id RETURN p"
        const queryUpdate = "MATCH(p:Profile) WHERE p.accountId = $id SET p.bio = $bio, p.favoriteBooks = $favoriteBooks, p.name = $name RETURN p"
        const id = req.accountId

        const rawCurrentInfo = await session.run(querySearch, {id: id})
        const account = {
            ...rawCurrentInfo.records[0]._fields[0].properties,
            ...req.body,
            id: id
        }
        const rawResult = await session.run(queryUpdate, account)
        const result = {
            ...rawResult.records[0]._fields[0].properties
        }
        res.status(200).json(result)
    }
    catch(err){
        next(err)
    }

}

async function getAll(req, res, next){
    try{
        const session = neo.session()
        const query = "match (p:Profile) return p"
        let rawResult = await session.run(query)
        let result = []

        rawResult.records.forEach(element => {
            let item = {
                ...element._fields[0].properties
            }
            result.push(item)            
        });
        res.status(200).json(result)
    }
    catch(err){
        next(err)
    }

}

async function getById(req, res, next){
    try{
        const session = neo.session()
        const id = req.params.id
        const queryProfile = "match(p:Profile) WHERE p.accountId = $id return p"
        const queryFollowers = "match(a:Profile)<-[r:FOLLOWS]-(b:Profile) where a.accountId = $id return collect(b)"
        const queryFollowing = "match(a:Profile)-[r:FOLLOWS]->(b:Profile) where a.accountId = $id return collect(b)"

        const rawProfile = await session.run(queryProfile, {id: id})
        
        let result = {
            ...rawProfile.records[0]._fields[0].properties,
            followers:  [],
            following: []
        }

        const rawFollowers = await session.run(queryFollowers, {id: id})
        await rawFollowers.records[0]._fields[0].forEach(async follower => {
            //Put all followers full info in the array.
            follower.properties.favoriteBooks = await Book.find({_id: follower.properties.favoriteBooks})
            result.followers.push(follower.properties)
        })
        const rawFollowing = await session.run(queryFollowing, {id: id})
        await rawFollowing.records[0]._fields[0].forEach(async following => {
            //Put all followed profiles full info in the array.
            following.properties.favoriteBooks = await Book.find({_id: following.properties.favoriteBooks})
            result.following.push(following.properties)
        })
        const favoriteBooks = await Book.find({_id: result.favoriteBooks})
        result.favoriteBooks = favoriteBooks
        res.status(200).json(result)
    }
    catch(err){
        next(err)
    }
}

async function followProfile(req, res, next){
    try{
        const session = neo.session()
        const query = "MATCH (a:Profile), (b:Profile) WHERE a.accountId = $a AND b.accountId = $b CREATE (a)-[r:FOLLOWS]->(b)"
        const profileA = req.accountId
        const profileB = req.params.id

        await session.run(query, {a: profileA, b: profileB})
        res.status(200).json({status: "Success", message: "Successfully followed user"})        
    }
    catch(err){
        next(err)
    }
}

async function unfollowProfile(req, res, next){
    try{
        const session = neo.session()
        const query = "MATCH (a:Profile)-[r:FOLLOWS]->(b:Profile) WHERE a.accountId = $a AND b.accountId = $b DELETE r"
        const profileA = req.accountId
        const profileB = req.params.id

        await session.run(query, {a: profileA, b: profileB})
        res.status(200).json({status: "Success", message: "Successfully unfollowed user"})        
    }
    catch(err){
        next(err)
    }

}

module.exports = {
    remove,
    getAll,
    followProfile,
    unfollowProfile,
    getById,
    update
}