
const { EntityNotFoundError, AccessDeniedError } = require("../errors");
const Book = require("../models/book.model")()
const BookCopy = require("../models/bookcopy.model")();

async function create(req, res, next){
    let body = {dateOfEntry: new Date(), ...req.body}
    const newBookCopy = new BookCopy(body)
    try{
        await newBookCopy.validate()
        const book = await Book.findById(newBookCopy.copyOf);
        if(book){
            const result = await newBookCopy.save()
            res.status(201).json(result)
        }
        else{
            throw new EntityNotFoundError("Book couldn't be found")
        }
    }
    catch(err){
        next(err)
    }
}

async function getAll(req, res, next){
    BookCopy.find(req.query)
        .then((result) => {
            res.status(200).json(result)
        })
        .catch((err) => {
            next(err)
        })
}

async function getById(req, res, next){
    const id = req.params.id
    BookCopy.findById(id)
        .then((result) => {
            if(result){
                res.status(200).json(result);
            }
            else{
                throw new EntityNotFoundError("BookCopy not found")
            }
        })
        .catch((err) => {
            next(err)
        })
}

async function remove(req, res, next){
    const id = req.params.id;
    BookCopy.findByIdAndDelete(id)
        .then((result) => {
            if(result){
                res.status(200).json(result)
            }
            else{
                throw new EntityNotFoundError("BookCopy not found")
            }
        })
        .catch((err) => {
            next(err)
        })
}

async function update(req, res, next){
    const id = req.params.id;
    try{
        const bookCopy = await BookCopy.findById(id);
        bookCopy.set(req.body)
        const book = await Book.findById(bookCopy.copyOf);
        if(book){
            const result = await bookCopy.save()
            res.status(201).json(result)
        }
        else{
            throw new EntityNotFoundError("Book couldn't be found")
        }
    }
    catch(err){
        next(err)
    }
}

async function unloan(req, res, next){
    const id = req.params.id;
    const accountId = req.accountId;
    try{
        const bookCopy = await BookCopy.findById(id);
        if(bookCopy){
            if(bookCopy.loanedTo == accountId){
                bookCopy.isCurrentlyLoaned = false;
                bookCopy.loanedTo = undefined
                await bookCopy.save()
                res.status(200).json({status: "Success", message: "You turned in the book."})
            }
            else{
                throw new EntityNotFoundError("You aren't currently loaning this book.")
            }
        }
        else{
            throw new EntityNotFoundError("Could not find book copy.")
        }
    }
    catch(err){
        next(err)
    }
}

async function loan(req, res, next){
    const id = req.params.id;
    const accountId = req.accountId;
    try{
        const bookCopy = await BookCopy.findById(id);
        if(bookCopy){
            if(bookCopy.canBeLoaned){
                if(!bookCopy.isCurrentlyLoaned){
                    bookCopy.isCurrentlyLoaned = true
                    bookCopy.loanedTo = accountId
                    await bookCopy.save();
                    res.status(200).json({status: "Success", message: "Book sucessfully loaned"})
                }
                else{
                    const message = bookCopy.loanedTo == accountId ? "You're already loaning this book" : "This book is currently being loaned."
                    throw new EntityNotFoundError(message)
                }
            }
            else{
                throw new EntityNotFoundError("This book copy cannot be loaned.")
            }
        }
        else{
            throw new EntityNotFoundError("Could not find book copy.")
        }
    }
    catch(err){
        next(err)
    }
}

module.exports = {
    create,
    getAll,
    getById,
    remove,
    update,
    loan,
    unloan
}