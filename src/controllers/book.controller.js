const { EntityNotFoundError } = require("../errors");
const Book = require("../models/book.model")();
const Writer = require("../models/writer.model")()

async function create(req, res, next){
    const newBook = new Book(req.body)
    try{
        await newBook.validate()
        const writer = await Writer.findById(newBook.writer);
        if(writer){
            const result = await newBook.save()
            res.status(201).json(result)
        }
        else{
            throw new EntityNotFoundError("Writer couldn't be found")
        }

    }
    catch(err){
        next(err)
    }
}

async function getAll(req, res, next){
    Book.find(req.query)
        .then((result) => {
            res.status(200).json(result)
        })
        .catch((err) => {
            next(err)
        })
}

async function getById(req, res, next){
    const id = req.params.id
    Book.findById(id)
        .then((result) => {
            if(result){
                res.status(200).json(result);
            }
            else{
                throw new EntityNotFoundError("Book not found")
            }
        })
        .catch((err) => {
            next(err)
        })
}

async function remove(req, res, next){
    const id = req.params.id;
    Book.findByIdAndDelete(id)
        .then((result) => {
            if(result){
                res.status(200).json(result)
            }
            else{
                throw new EntityNotFoundError("Book not found")
            }
        })
        .catch((err) => {
            next(err)
        })
}

async function update(req, res, next){
    const id = req.params.id;
    try{
        const book = await Book.findById(id);
        book.set(req.body)
        const writer = await Writer.findById(book.writer);
        if(writer){
            const result = await book.save()
            res.status(201).json(result)
        }
        else{
            throw new EntityNotFoundError("Writer couldn't be found")
        }
    }
    catch(err){
        next(err)
    }
}

module.exports = {
    create,
    getAll,
    getById,
    remove,
    update
}