const express = require('express')
const router = express.Router()

const authController = require("../controllers/authentication.controller")
const bookController = require("../controllers/book.controller")
router.get("/", bookController.getAll)
router.get("/:id", bookController.getById)
router.post("/", authController.verify, bookController.create)
router.put("/:id", authController.verify, bookController.update)
router.delete("/:id", authController.verify, bookController.remove)

module.exports = router