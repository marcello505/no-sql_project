const chai = require('chai')
const expect = chai.expect

const requester = require('../../requester.spec')

describe("Profile endpoints", function(){
    describe("integration tests", function(){
        it("(POST /auth/register) should also create a profile", async function(){
            const testAccount = {
                email: "test@account.com",
                password: "password"
            }

            const authRes = await requester.post("/auth/register").send(testAccount)

            expect(authRes).to.have.status(201)
            const profileRes = await requester.get("/profile").send()
            expect(profileRes).to.have.status(200)
            expect(profileRes.body[0]).to.have.property("name")
            expect(profileRes.body[0]).to.have.property("bio")
        })

        it("(POST /profile/follow) should lay down a follow relation", async function(){
            //This test somehow fails despite the usage of done
            const testAccountA = {
                email: "testA@account.com",
                password: "password"
            }
            const testAccountB = {
                email: "testB@account.com",
                password: "password"
            }
            let authRes = await requester.post("/auth/register").send(testAccountA)
            expect(authRes).to.have.status(201)
            authRes = await requester.post("/auth/register").send(testAccountB)
            expect(authRes).to.have.status(201)
            authRes = await requester.post("/auth/login").send(testAccountA)
            expect(authRes).to.have.status(200)
            const token = authRes.body.token
            const _id = authRes.body.account._id
            let profileRes = await requester.get("/profile").send()
            expect(profileRes).to.have.status(200)
            let otherId;
            profileRes.body.forEach(item => {
                if(item.accountId != _id){
                    otherId = item.accountId
                }
            })
            profileRes = await requester.post(`/profile/follow/${otherId}`).set({ Authorization: `Bearer ${token}` }).send()
            expect(profileRes).to.have.status(200)
            profileRes = await requester.get(`/profile/${_id}`).send()
            expect(profileRes).to.have.status(200)
            expect(profileRes.body.following[0]).to.have.property("accountId").and.to.be.equal(otherId)
            })
})
})