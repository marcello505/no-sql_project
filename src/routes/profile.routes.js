const express = require('express')
const router = express.Router()

const authController = require("../controllers/authentication.controller")
const profileController = require("../controllers/profile.controller")
router.get("/", profileController.getAll)
router.get("/:id", profileController.getById)
router.delete("/", authController.verify, profileController.remove)
router.put("/", authController.verify, profileController.update)
router.post("/follow/:id", authController.verify, profileController.followProfile)
router.post("/unfollow/:id", authController.verify, profileController.unfollowProfile)

module.exports = router