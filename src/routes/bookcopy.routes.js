const express = require('express')
const router = express.Router()

const authController = require("../controllers/authentication.controller")
const bookCopyController = require("../controllers/bookcopy.controller")
router.get("/", bookCopyController.getAll)
router.get("/:id", bookCopyController.getById)
router.post("/", authController.verify, bookCopyController.create)
router.put("/:id", authController.verify, bookCopyController.update)
router.delete("/:id", authController.verify, bookCopyController.remove)
router.post("/loan/:id", authController.verify, bookCopyController.loan)
router.post("/unloan/:id", authController.verify, bookCopyController.unloan)

module.exports = router