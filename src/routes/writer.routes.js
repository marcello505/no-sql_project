const express = require("express")
const router = express.Router();

const writerController = require("../controllers/writer.controller")
router.post("/", writerController.create)
router.get("/", writerController.getAll)
router.get("/:writerId", writerController.getById)
router.delete("/:writerId", writerController.remove)
router.put("/:writerId", writerController.update)

module.exports = router