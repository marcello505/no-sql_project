const express = require("express")
const router = express.Router()

const authenticationController = require("../controllers/authentication.controller")
router.post("/register", authenticationController.register)
router.post("/login", authenticationController.login)
router.get("/verify", authenticationController.userVerify)

module.exports = router