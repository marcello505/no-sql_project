const mongoose = require('mongoose');
const getModel = require('./model_cache')
const Schema = mongoose.Schema;

const writerSchema = new Schema({
  name: { type: String, required: true },
  description: { type: String, required: true},
  dateOfBirth: { type: Date, required: true },
});


module.exports = getModel("Writer", writerSchema);
