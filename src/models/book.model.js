const mongoose = require('mongoose');
const getModel = require('./model_cache')
const Schema = mongoose.Schema;

const bookSchema = new Schema({
  title: { type: String, required: true },
  ISBN: { type: String, required: true, maxlength: 13 },
  description: { type: String, required: true, maxLength: 250},
  releaseDate: { type: Date, required: true },
  writer: { type: Schema.Types.ObjectId, ref: "Writer", required: true, autopopulate: true},
  publisher: { type: String, required: true },
  language: {type: String, required: true}
});

bookSchema.plugin(require('mongoose-autopopulate'));

module.exports = getModel("Book", bookSchema);
