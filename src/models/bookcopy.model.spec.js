const chai = require("chai")
const expect = chai.expect

var chaiAsPromised = require("chai-as-promised")
chai.use(chaiAsPromised)

const Writer = require("./writer.model")()
const Book = require("./book.model")()
const BookCopy = require("./bookcopy.model")()

describe("BookCopy Model", function(){
    describe("Unit tests", function(){
        it("should have all its fields required", async function(){
            const writer =  new Writer({name: "Dr. Seuss", description: "Blah", dateOfBirth: "2012-01-01"})
            await writer.save()
            const book = new Book({
                title: "The Lorax",
                ISBN: "9780007455935",
                description: "This is a childrens book",
                releaseDate: "2012-01-01",
                writer: writer._id,
                publisher: "HarperCollins Publishers",
                language: "English"
            })
            await book.save()
            const bookCopy = new BookCopy()
            await expect(bookCopy.validate()).to.be.rejectedWith(Error)
            bookCopy.set({copyOf: book._id, canBeLoaned: false, isCurrentlyLoaned: false, dateOfEntry: "2013-01-01"})
            await expect(bookCopy.validate()).to.not.be.rejectedWith(Error)
        })
        it("should automatically populate it's copyOf field", async function(){
            const writer =  new Writer({name: "Dr. Seuss", description: "Blah", dateOfBirth: "2012-01-01"})
            await writer.save()
            const book = new Book({
                title: "The Lorax",
                ISBN: "9780007455935",
                description: "This is a childrens book",
                releaseDate: "2012-01-01",
                writer: writer._id,
                publisher: "HarperCollins Publishers",
                language: "English"
            })
            await book.save()
            const bookCopy = new BookCopy({copyOf: book._id, canBeLoaned: false, isCurrentlyLoaned: false, dateOfEntry: "2013-01-01"})
            await bookCopy.save()
            expect(bookCopy.copyOf).to.be.an("Object")
        })
    })
})