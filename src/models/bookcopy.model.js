const mongoose = require('mongoose');
const getModel = require('./model_cache')
const Schema = mongoose.Schema;

const bookCopySchema = new Schema({
  copyOf: { type: Schema.Types.ObjectId, ref: "Book", required: true, autopopulate: true},
  canBeLoaned: { type: Boolean, required: true},
  isCurrentlyLoaned: {type: Boolean, required: true},
  loanedTo: { type: Schema.Types.ObjectId, ref: "Account"},
  dateOfEntry: { type: Date, required: true },
});

bookCopySchema.plugin(require('mongoose-autopopulate'));

module.exports = getModel("BookCopy", bookCopySchema);
