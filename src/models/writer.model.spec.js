
const chai = require('chai')
const expect = chai.expect

var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

const Writer = require("./writer.model")()

describe("Writer Model", function(){
    describe("Unit tests", function(){
        it("should have all it's fields required", async function(){
            const writer = new Writer({})
            await expect(writer.validate()).to.be.rejectedWith(Error)
            writer.name = "Joe"
            await expect(writer.validate()).to.be.rejectedWith(Error)
            writer.description = "A great writer"
            await expect(writer.validate()).to.be.rejectedWith(Error)
            writer.dateOfBirth = "1998-11-30"
            await expect(writer.validate()).to.not.be.rejectedWith(Error)
        })
    })
})