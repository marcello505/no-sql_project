const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const UniqueValidator = require("mongoose-unique-validator")
const getModel = require('./model_cache')

const accountSchema = new Schema({
    email: {type: String, required: true, unique: true, validate: /^\S+@\S+\.\S+$/},
    password: {type: String, required: true, minlength: 6}
});

// Apply unique validator plugin
accountSchema.plugin(UniqueValidator)


module.exports = getModel("Account", accountSchema)