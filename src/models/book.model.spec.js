const chai = require("chai")
const expect = chai.expect

var chaiAsPromised = require("chai-as-promised")
chai.use(chaiAsPromised)

const Writer = require("./writer.model")()
const Book = require("./book.model")()

describe("Book Model", function(){
    describe("Unit tests", function(){
        it("should have all its fields required", async function(){
            const book = new Book({})
            await expect(book.validate()).to.be.rejectedWith(Error)
            const writer =  new Writer({name: "Dr. Seuss", description: "Blah", dateOfBirth: "2012-01-01"})
            await writer.save()
            book.set({
                title: "The Lorax",
                ISBN: "9780007455935",
                description: "This is a childrens book",
                releaseDate: "2012-01-01",
                writer: writer._id,
                publisher: "HarperCollins Publishers",
                language: "English"
            })
            await expect(book.validate()).to.not.be.rejectedWith(Error)
        })
        it("should automatically populate it's writers field", async function(){
            const book = new Book({})
            const writer =  new Writer({name: "Dr. Seuss", description: "Blah", dateOfBirth: "2012-01-01"})
            await writer.save()
            book.set({
                title: "The Lorax",
                ISBN: "9780007455935",
                description: "This is a childrens book",
                releaseDate: "2012-01-01",
                writer: writer._id,
                publisher: "HarperCollins Publishers",
                language: "English"
            })
            await book.save()
            expect(book.writer).to.be.an("Object")
        })
    })
})