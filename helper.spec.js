// reads the .env file and stores it as environment variables, use for config
require('dotenv').config()

const connect = require('./connect')

const Account = require("./src/models/account.model")()
const BookCopy = require("./src/models/bookcopy.model")()
const Writer = require("./src/models/writer.model")()
const Book = require("./src/models/book.model")()

const neo = require('./neo')

// connect to the databases
connect.mongo(process.env.MONGO_TEST_DB)
connect.neo(process.env.NEO4J_TEST_DB)

beforeEach(async () => {
    // drop both collections before each test
    await Promise.all([Account.deleteMany(), BookCopy.deleteMany(), Writer.deleteMany(), Book.deleteMany()])

    // clear neo db before each test
    const session = neo.session()
    await session.run(neo.dropAll)
    await session.close()
});